** ReadMe Documentation** 

**Why software testing?** 

In simple words, Software testing is really required to point out the defects and errors that were made during the development phases.

**Benefits:**
 
•	In sample language unit test helps us to find the bugs early.  
•	It makes complex code easy to understand: reading source code of the testing can highlight the intended functions of the code.  
•	It helps to save the time, easier to change and refactor the code  
•	In short, software test adds a great value to the applications 


**AUTOMATED TESTING:** 

•	Automated testing is done through an automation tool, less time is   needed in exploratory tests and more time is needed in maintaining test scripts while increasing overall test coverage.           
•	Automated testing is well-suited for large projects.

**Manual Testing:**

•	Time – consuming and repetitive
•	Human being is responsible for single-handedly testing the functionality of the software 

**Automated Unit Testing:** 

•	It can be performed at any time
•	It pays attention to the behavior of single modules
•	It is usually executed by developer
•	Automated Unit test is a function that tests a unit of work (aka module, component)

**NOTE**: *The idea behind unit testing is to test each part of the program and show that the individual parts are correct.*

**Unit testing Framework:**
 
**Microsoft Unit Testing Framework (also known as MSTest):** 

•	MSTest is integrated with the visual studio that means we don't have to install any third-party tools 

**xUnit.net Testing Framework:** xUnit is a free, open source unit testing framework and is more modern than the built in MSTest.


**Integration testing:**
 
•	It’s a test that touches multiple components. It uses the real objects but sometimes it can be mocked.                               
•	It is a software testing technique where individual units of a program are combined and tested as a group.                          
•	The idea behind Integration testing is to combine modules in the application and test as a group to see that they are working fine       
•	It usually carried out after Unit Testing and before System Testing
•	It pays attention to integration among modules                   
•	It is usually executed by test team


**Mocking:** Mocking is creating (fake) objects that simulate the behavior of real objects.

•	Mocking is primarily used in unit testing.                    
•	To isolate the behavior of the object you want to test you replace    the other objects by mocks that simulate the behavior of the real objects.  
•	This is useful if the real objects are impractical to incorporate into the unit test.                                                    
•	Best way to check objects with multiple variations or combinations of results of its methods, etc.

**Isolation Framework:** It only provides predefined outputs, and thus isolates "test" from external system with complex internal state, slow response time, etc. Ex: stub

**Mock Frameworks:** It not only provides predefined outputs if needed, but also has pre-programmed with expectations which form a specification of the calls they are expected to receive. 

**MOQ:**  It is a mocking framework for C#/.NET. It is used in unit testing to isolate your class under test from its dependencies and ensure that the proper methods on the dependent objects are being called.

**Wallaby.js:** A Framework used in JavaScript testing. Wallaby.js runs tests as you type and displays its results inside the editor code editor giving the user real time feedback Wallaby.js can be installed as a plug-in for various editors including Visual Studio.

**Jasmine Tutorial** 
Obtain Jasmine by downloading the available zip file. (https://github.com/jasmine/jasmine/releases)

Unzip folder

Delete files in /src and /spec directory after unzipping. The files included are for demonstration purposes. (Player.js and Song.js)

The JavaScript file to be tested should be placed in the /src directory.![img2.png](https://bitbucket.org/repo/BLzrbM/images/1649380819-img2.png) 

A JavaScript file needs to be created to let Jasmine know which file needs to be inspected.![img1.png](https://bitbucket.org/repo/BLzrbM/images/4000618260-img1.png)

Next the SpecRunner HTML page needs to be edited to match your files which need to be tested.![img.png](https://bitbucket.org/repo/BLzrbM/images/1410461852-img.png)  

Once the Spec HTML page is edited, simply open the HTML page and it will run and check the source files.